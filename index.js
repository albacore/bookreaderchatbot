import { config } from "dotenv";
import { Telegraf, Markup } from "telegraf";
import text from "./books/brothers-karamazov.js";
import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync.js";

const adapter = new FileSync("db.json");
const db = low(adapter);

config();

db.defaults({ users: {} }).write();

const bot = new Telegraf(process.env.TELEGRAM_TOKEN);

const textParts = text.split(".");

bot.command("books", (ctx) => {
  ctx.reply(`Right now we only have Brothers Karamazov `);
});

// define a next button
const nextButton = Markup.inlineKeyboard([
  Markup.button.callback("Next", "NEXT"),
]);

const getCurrentSpot = (ctx) =>
  db.get("users." + ctx.chat.id + ".spotInTheBook").value();

const setSpotInTheBook = (ctx, spot) =>
  db.set("users." + ctx.chat.id + ".spotInTheBook", spot).write();

bot.command("where", (ctx) => {
  ctx.reply("You are on sentence number " + getCurrentSpot(ctx));
});
bot.command("help", (ctx) => {
  ctx.reply(
    `/start to start reading a book sentence by sentence. /restart to start reading from the beginning again. /where to see how many sentences you have read already.`
  );
});

bot.command("quit", (ctx) => {
  // Explicit usage
  ctx.telegram.leaveChat(ctx.message.chat.id);

  // Using context shortcut
  ctx.leaveChat();
});

bot.action("NEXT", (ctx) => {
  sendNextSentence(getCurrentSpot(ctx), textParts, ctx, nextButton);
});

const sendNextSentence = (spotInTheBook, textParts, ctx, nextButton) => {
  typeof textParts[spotInTheBook] === 'string'
    ? ctx.reply(textParts[spotInTheBook] + ".", nextButton)
    : ctx.reply(`done`);

  db.update("users." + ctx.chat.id + ".spotInTheBook", (n) => n + 1).write();
};

bot.command("start", (ctx) => {
  if (getCurrentSpot(ctx)) {
    ctx.reply(
      "You are in the middle of the book. If you'd like to start from the beginning again type this command /restart otherwise press Next to continue reading from where you were before.",
      nextButton
    );
    return;
  }
  setSpotInTheBook(ctx, 0);
  sendNextSentence(0, textParts, ctx, nextButton);
});

bot.command("restart", (ctx) => {
  setSpotInTheBook(ctx, 0);
  sendNextSentence(0, textParts, ctx, nextButton);
});

const stats = (db) => {
  const users = db.get("users").value();
  const usersCount = Object.keys(users).length;
  const totalSentencesRead = Object.values(users).reduce(
    (a, b) => a + b.spotInTheBook,
    0
  );
  return { usersCount, totalSentencesRead };
};

bot.command("stats", (ctx) => {
  const { usersCount, totalSentencesRead } = stats(db);
  ctx.reply(
    usersCount +
      " users have read " +
      totalSentencesRead +
      " sentences in total so far."
  );
});

bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
