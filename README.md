# BookReaderChatBot

This is a telegram chatbot to read books as telegram messages, one sentence at a time. 

## Install

You need to have node installed. 

```
npm i
node index.js
```
