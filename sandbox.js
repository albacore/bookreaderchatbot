import text from "./books/brothers-karamazov.js";
import low from "lowdb";
import FileSync from "lowdb/adapters/FileSync.js";

const adapter = new FileSync("db.json");
const db = low(adapter);


db.defaults({ users: {} }).write();

const textParts = text.split(".");


const getCurrentSpot = (ctx) =>
  db.get("users." + ctx.chat.id + ".spotInTheBook").value();

const setSpotInTheBook = (ctx, spot) =>db.set("users." + ctx.chat.id + ".spotInTheBook", spot).write();

const stats = (db) => {
  const users = db.get('users').value();
  const usersCount = Object.keys(users).length;
  const totalSentencesRead = Object.values(users).reduce((a, b) =>  a + b.spotInTheBook, 0)
  return {usersCount, totalSentencesRead} 
}

console.log(stats(db));
